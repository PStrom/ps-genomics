#! /usr/bin/env python3

import argparse
import os
import sys


class Main:
    @staticmethod
    def get_lines_from_file(file_name):
        with open(file_name) as file:
            return [next(file) for x in range(2)]

    @staticmethod
    def run():
        arg_parser = argparse.ArgumentParser(
            description='Adds the first two rows from files listed in an index file to a new file.')
        arg_parser.add_argument(
            'file',
            type=str,
            help='The index file that contains the list of files (proteins).'
        )
        if len(sys.argv) == 1:
            arg_parser.print_help()
            sys.exit(1)

        args = arg_parser.parse_args()
        with open(args.file, 'r') as file:
            lines_with_data = [l.strip() for l in file if l[0] != '#']

        read_path, read_file_name = os.path.split(args.file)

        output_file_name = "{}{}".format('_', read_file_name)
        output_path = os.path.join(read_path, output_file_name)

        with open(output_path, 'w') as file:
            for file_name in lines_with_data:
                lines_in_file = Main.get_lines_from_file(os.path.join(read_path, file_name))
                for line in lines_in_file:
                    file.write(line)

        print("Generated new file: {}".format(file_name))


if __name__ == "__main__":
    Main.run()
