# ps-genomics #

Scripts for making some various genomics operations a bit simpler.


## Prerequisites ##

`python3.5` or higher to be installed, and on the path.


## Installation -linux ##


Clone this repository to a convenient folder  
```git@bitbucket.org:PStrom/ps-genomics.git```  
and add it to the path (for example .bashrc and/or .bash_profile)

make the scripts executable

_example:_
```
chmod +x ps-amali.py
```


## How to use ##

### ps-amali ###


```
ps-amali.py filename
```
*Replace "filename" with the name of the file containing the proteins.

## Author ##
Peder Str�mberg <pederstromberg@gmail.com>

## Licence ##
MIT

Copyright 2017 Peder Str�mberg

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
